package com.sky.tank;

import com.sky.tank.data.GameWindowData;
import com.sky.tank.data.ImageIconData;
import com.sky.tank.exception.ImageIconException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author sky
 * @version 1.0
 * @date 2022/5/16 23:43:11
 */

@SpringBootTest
public class TestComponent {

    @Autowired
    private GameWindowData gameWindowData;

    @Test
    void Test(){
        System.out.println(gameWindowData);
    }


}

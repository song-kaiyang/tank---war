package com.sky.tank.enums;

/**
 * 坦克的移动方向(包含四个方向)
 * <ul>
 *     <li>上</li>
 *     <li>下</li>
 *     <li>左</li>
 *     <li>右</li>
 * </ul>
 * @author sky
 * @version 1.0
 * @date 2022/5/16 23:14:01
 */

public enum DirectionTank {
    /**
     * 方向向左，枪管朝向左
     */
    LEFT("left"),

    /**
     * 方向向右，枪管朝向右
     */
    RIGHT("right"),

    /**
     * 方向向下，枪管朝向下
     */
    DOWN("down"),

    /**
     * 方向向上，枪管朝向上
     */
    UP("up");

    private final String value;
    DirectionTank(String value){
        this.value = value;
    }

    public String value(){
        return this.value;
    }
}

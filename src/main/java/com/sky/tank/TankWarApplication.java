package com.sky.tank;

import com.sky.tank.panel.SwingArea;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.swing.*;
import java.awt.*;

/**
 * @author sky
 * @version 1.0
 * @date 2022/5/16 21:57:18
 */

@SpringBootApplication
public class TankWarApplication {

    public static void main(String[] args) {

        SwingUtilities.invokeLater(()->{
            SwingArea.getInstance().initUi();
        });

        SpringApplication.run(TankWarApplication.class,args);
    }


}

package com.sky.tank.data;

import com.sky.tank.enums.DirectionTank;
import com.sky.tank.exception.ImageIconException;


import javax.imageio.ImageIO;
import javax.swing.*;

import java.io.IOException;

import java.util.Optional;

/**
 * @author sky
 * @version 1.0
 * @date 2022/5/16 21:58:17
 */

public class ImageIconData {

    private final static String TANK_IMAGE_PATH = "/static/tank/";
    private final static String BATTLE_IMAGE_PATH = "/static/battle/";
    private final static String BULLET_IMAGE_PATH = "/static/bullet/";
    private final static String OWN_TANK_PREFIX = "yellow";
    private final static String IMAGE_SEPARATOR = "-";

    public final static ImageIcon YELLOW_LEFT = getTankImageIcon("yellow-left.gif");
    public final static ImageIcon YELLOW_RIGHT = getTankImageIcon("yellow-right.gif");
    public final static ImageIcon YELLOW_UP = getTankImageIcon("yellow-up.gif");
    public final static ImageIcon YELLOW_DOWN = getTankImageIcon("yellow-down.gif");

    public final static ImageIcon GREY_LEFT = getTankImageIcon("grey-left.gif");
    public final static ImageIcon GREY_RIGHT = getTankImageIcon("grey-right.gif");
    public final static ImageIcon GREY_UP = getTankImageIcon("grey-up.gif");
    public final static ImageIcon GREY_DOWN = getTankImageIcon("grey-down.gif");

    public final static ImageIcon STONE_1 = getBattleImageIcon("shi-1.gif");
    public final static ImageIcon STONE_H = getBattleImageIcon("shi-h.gif");
    public final static ImageIcon STONE_V = getBattleImageIcon("shi-V.gif");
    public final static ImageIcon STONE_ALL = getBattleImageIcon("shi-all.gif");
    public final static ImageIcon WALL = getBattleImageIcon("wall.gif");
    public final static ImageIcon WALLS = getBattleImageIcon("walls.gif");
    public final static ImageIcon OWN_BOSS = getBattleImageIcon("boss.png");

    public final static ImageIcon BULLET_OWN = getBulletImageIcon("own.gif");



    public static ImageIcon getResourceIcon(String fileName){
        try {
            return Optional.ofNullable(ImageIconData.class.getResourceAsStream(fileName))
                    .map(i -> {
                        try {
                            return ImageIO.read(i);
                        } catch (IOException e) {
                            return null;
                        }
                    }).map(ImageIcon::new)
                    .orElseThrow(() -> new ImageIconException("没有找到文件" + fileName));
        }catch (ImageIconException e){
            e.printStackTrace();
        }
        return new ImageIcon();
    }

    public static ImageIcon getTankImageIcon(String tankFile){
        return getResourceIcon(TANK_IMAGE_PATH+tankFile);
    }

    public static ImageIcon getBattleImageIcon(String battleFile){
        return getResourceIcon(BATTLE_IMAGE_PATH+battleFile);
    }

    public static ImageIcon getBulletImageIcon(String bulletFile){
        return getResourceIcon(BULLET_IMAGE_PATH+bulletFile);
    }


    public static ImageIcon getOwnTank(DirectionTank direction){
        ImageIcon icon;
        switch (direction){
            case UP: icon=YELLOW_UP;break;
            case DOWN: icon=YELLOW_DOWN;break;
            case LEFT: icon=YELLOW_LEFT;break;
            case RIGHT: icon=YELLOW_RIGHT;break;
            default:
                icon = new ImageIcon();
        }
        return icon;
    }

    public static ImageIcon getGreyTank(DirectionTank directionTank){
        ImageIcon icon;
        switch (directionTank){
            case UP: icon=GREY_UP;break;
            case DOWN: icon=GREY_DOWN;break;
            case LEFT: icon=GREY_LEFT;break;
            case RIGHT: icon=GREY_RIGHT;break;
            default:
                icon = new ImageIcon();
        }
        return icon;
    }

}

package com.sky.tank.data;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @author sky
 * @version 1.0
 * @date 2022/5/16 23:33:07
 */

@Data
@Component
@PropertySource(value = "classpath:game.properties")
@ConfigurationProperties(prefix = "window")
public class GameWindowData {
    private int height;
    private int width;
}

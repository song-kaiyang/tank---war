package com.sky.tank.panel;

import com.sky.tank.basic.BaseWar;

import javax.swing.*;
import java.awt.*;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * @author sky
 * @version 1.0
 * @date 2022/5/16 22:34:07
 */
public class SwingArea extends JFrame{
    private static SwingArea instance;


    public static SwingArea getInstance(){
        if(Objects.isNull(instance)){
            synchronized (SwingArea.class){
                if(Objects.isNull(instance)){
                    instance = new SwingArea();
                }
            }
        }
        return instance;
    }



    public void initUi(){

        BaseWar war = War.instance;
        setContentPane(war);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setSize(900,900);
        this.setTitle("Tank War");
//        this.setResizable(false);
        setLocationRelativeTo(null);
        this.setVisible(true);
    }
}

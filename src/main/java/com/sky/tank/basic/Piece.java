package com.sky.tank.basic;

import com.sky.tank.data.GameWindowData;
import com.sky.tank.data.ImageIconData;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.imageio.ImageIO;
import javax.swing.*;

/**
 * @author sky
 * @version 1.0
 * @date 2022/5/18 15:16:24
 */

@Data
@AllArgsConstructor
@Accessors(chain = true)
public class Piece {
    public int x;
    public int y;
    public boolean canDestroy;
    public boolean isIgnore;
    public int width;
    public int height;
    public ImageIcon pic;

    public Piece(int x,int y,int width,int height){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.canDestroy = true;
        this.isIgnore = true;
    }

    public static Piece initWall(int x, int y, int width, int height){
        return new Piece(x,y,false,false,width,height, new ImageIcon());
    }
}

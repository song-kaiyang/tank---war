package com.sky.tank.basic;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 一格20像素
 * @author sky
 * @version 1.0
 * @date 2022/5/16 21:58:41
 */

@Data
@EqualsAndHashCode(callSuper = true)
public abstract class BaseWar extends JPanel {


    protected int latticeX;
    protected int latticeY;
    protected int lattice;
    protected List<Piece> battle;

    /**
     * 上边距
     */
    protected int top;
    /**
     * 下边距
     */
    protected int left;

    protected Tank tank;

    protected BaseWar(){
        this.latticeX = 13;
        this.latticeY = 13;
        this.lattice = 60;
    }

    public BaseWar(int latticeX, int latticeY, int lattice) {
        this.latticeX = latticeX;
        this.latticeY = latticeY;
        this.lattice = lattice;
    }

    /**
     * 加载地图
     */
    public void loadWall(){
        battle = new ArrayList<>();
    }

    public abstract void tankMove();



}

package com.sky.tank.basic;

import com.sky.tank.data.ImageIconData;
import com.sky.tank.enums.DirectionTank;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.swing.*;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author sky
 * @version 1.0
 * @date 2022/5/17 15:34:55
 */


@Setter
@Accessors(chain = true)
public class Tank {
    public DirectionTank direction;
    /**
     * 一秒走几格
     */
    public int speed;

    public int x;
    public int y;

    public int width;
    public int height;

    public Bullet bullet;

    /**
     *    用于自动
     */
    public Timer bodyMove;

    /**
     * 子弹移动
     */
    public Timer bulletMove;

    /**
     * 现场子弹
     */
    public List<Tank.Bullet> bullets;


    /**
     * 炮弹装填时间
     */
    public long fillingTime;

    private Tank(){
        direction = DirectionTank.UP;
        width = 60;
        height = 60;
        speed  = 4;
        fillingTime = TimeUnit.MILLISECONDS.toMillis(500);
    }

    public Tank(int speed){
        this();
        this.speed = speed;
    }



    public static class Bullet{
        public int x;
        public int y;
        public int width;
        public int height;
        public int speed;
        public DirectionTank direction;
        public ImageIcon img;
        Tank tank;

        public Bullet(int width,int height,int speed,ImageIcon img){
            this.width = width;
            this.height = height;
            this.speed = speed;
            this.img = img;
        }

        public Bullet setTank(Tank tank){
            this.tank =tank;
            return this;
        }

        public Bullet initLocation(){
            Bullet bullet = new Bullet(width,height,speed,img);
            bullet.direction = tank.direction;
            switch (bullet.direction){
                case UP:
                    bullet.x = (tank.x + (tank.width - this.width)/2);
                    bullet.y = (tank.y - this.height);
                    break;
                case DOWN:
                    bullet.x = (tank.x + (tank.width - this.width)/2);
                    bullet.y = tank.y + tank.height;
                    break;
                case LEFT:
                    bullet.x = tank.x - bullet.width;
                    bullet.y = tank.y + (tank.width - bullet.width)/2;
                    break;
                case RIGHT:
                    bullet.x = tank.x + tank.width;
                    bullet.y = tank.y + (tank.width - bullet.width)/2;
                    break;
                default:
            }

            return bullet;
        }

        public void move(){
            switch (direction){
                case LEFT:
                    x -= speed;break;
                case RIGHT:
                    x += speed; break;
                case UP:
                    y-= speed;break;
                case DOWN:
                    y+= speed;break;
                default:
            }
        }

    }

}
